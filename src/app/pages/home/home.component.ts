import { Component, OnInit } from '@angular/core';

import { Task } from 'src/app/models/task.model';
import { AlertsService } from 'src/app/services/alerts.service';
import { CRUDService } from 'src/app/services/crud.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styles: [],
})
export class HomeComponent implements OnInit {
  public taskList: Task[] = [];

  constructor(
    private _CRUDService: CRUDService,
    private _alertsService: AlertsService
  ) {}

  ngOnInit(): void {
    this.taskList = this._CRUDService.getAllTasks();
  }

  deleteTask(id: number) {
    const message = '¿Desea eliminar esta tarea?';

    this._alertsService.showQuestionALert(message).then((result: any) => {
      if (result['isConfirmed']) {
        this._alertsService.showSuccessAlert('Tarea Eliminada!');

        this._CRUDService.deleteTaskById(id);
        this.taskList = this._CRUDService.getAllTasks();
      }
    });
  }

  deleteAllTasks() {
    const message = '¿Desea eliminar todas las tareas?';

    this._alertsService.showQuestionALert(message).then((result: any) => {
      if (result['isConfirmed']) {
        this._alertsService.showSuccessAlert('Tareas Eliminadas!');

        this._CRUDService.deleteAllTasks();
        this.taskList = this._CRUDService.getAllTasks();
      }
    });
  }
}
