import { Component } from '@angular/core';

import { Task } from 'src/app/models/task.model';

@Component({
  selector: 'app-create-task',
  templateUrl: './create-task.component.html',
})
export class CreateTaskComponent {
  public task: Task = new Task();

  constructor() {}
}
