import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { Task } from 'src/app/models/task.model';
import { CRUDService } from 'src/app/services/crud.service';

@Component({
  selector: 'app-update-task',
  templateUrl: './update-task.component.html',
})
export class UpdateTaskComponent implements OnInit {
  public task: Task = new Task();
  public taskId: number;

  constructor(
    private _activatedRoute: ActivatedRoute,
    private _CRUDService: CRUDService
  ) {
    this._activatedRoute.params.subscribe((params) => {
      this.taskId = Number(params['id']);
    });
  }

  ngOnInit(): void {
    this.task = this._CRUDService.getTaskById(this.taskId);
  }
}
