import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';

import { HomeComponent } from './pages/home/home.component';
import { CreateTaskComponent } from './pages/create-task/create-task.component';
import { UpdateTaskComponent } from './pages/update-task/update-task.component';

import { NavbarComponent } from './components/shared/navbar/navbar.component';
import { TaskCardComponent } from './components/task-card/task-card.component';
import { AlertCardComponent } from './components/alert-card/alert-card.component';
import { FormComponent } from './components/shared/form/form.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    CreateTaskComponent,
    UpdateTaskComponent,
    NavbarComponent,
    TaskCardComponent,
    AlertCardComponent,
    FormComponent,
  ],
  imports: [BrowserModule, AppRoutingModule, FormsModule],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
