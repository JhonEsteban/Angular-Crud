import { Injectable } from '@angular/core';

import { Task } from '../models/task.model';

@Injectable({
  providedIn: 'root',
})
export class CRUDService {
  private taskList: Task[] = [];

  constructor() {
    this.getTaskListFromLocalStorage();
  }

  private getTaskListFromLocalStorage(): void {
    if (localStorage.getItem('taskList')) {
      this.taskList = JSON.parse(localStorage.getItem('taskList') || '[]');
    } else {
      this.taskList = [];
    }
  }

  private saveTaskListInLocalStorage(): void {
    localStorage.setItem('taskList', JSON.stringify(this.taskList));
  }

  getAllTasks(): Task[] {
    return this.taskList;
  }

  addNewTask(task: Task): void {
    const newTask: Task = {
      ...task,
      id: new Date().getTime(),
    };

    this.taskList = [...this.taskList, newTask];
    this.saveTaskListInLocalStorage();
  }

  getTaskById(taskId: number): Task {
    return this.taskList.filter((task) => task.id === taskId)[0];
  }

  updateTask(newTask: Task): void {
    this.taskList.map((task) => (task.id === newTask.id ? newTask : task));
    this.saveTaskListInLocalStorage();
  }

  deleteTaskById(taskId: number): void {
    this.taskList = this.taskList.filter((task) => task.id !== taskId);
    this.saveTaskListInLocalStorage();
  }

  deleteAllTasks(): void {
    this.taskList = [];
    this.saveTaskListInLocalStorage();
  }
}
