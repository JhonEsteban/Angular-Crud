import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Router } from '@angular/router';

import { Task } from 'src/app/models/task.model';

@Component({
  selector: 'app-task-card',
  templateUrl: './task-card.component.html',
  styles: [],
})
export class TaskCardComponent {
  @Input() task: Task;
  @Output() eventDeleteTask: EventEmitter<number>;

  constructor(private _router: Router) {
    this.eventDeleteTask = new EventEmitter();
  }

  goToUpdateTask() {
    this._router.navigate(['/updatedTask', this.task.id]);
  }

  deleteTask(taskId: number) {
    this.eventDeleteTask.emit(taskId);
  }
}
