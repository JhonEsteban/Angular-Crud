import { Component, Input } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';

import { Task } from 'src/app/models/task.model';
import { AlertsService } from 'src/app/services/alerts.service';
import { CRUDService } from 'src/app/services/crud.service';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
})
export class FormComponent {
  @Input() task: Task;
  @Input() isForToUpdate: boolean;

  constructor(
    private _CRUDService: CRUDService,
    private _alertsService: AlertsService,
    private _router: Router
  ) {}

  createOrUdateTask(form: NgForm) {
    if (form.invalid) {
      return;
    }

    if (this.isForToUpdate) {
      this._CRUDService.updateTask(this.task);
      this._alertsService.showSuccessAlert('Se actualizo con exito');
    } else {
      this._CRUDService.addNewTask(this.task);
      this._alertsService.showSuccessAlert('Se Creo con exito');
    }

    this._router.navigate(['/home']);
  }

  toggleTaskDone() {
    this.task.done = !this.task.done;
  }
}
