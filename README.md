# Angular Crud 💻💼

## Desktop design

![](https://i.imgur.com/GENk9Ju.png)

## To run the project: 👇

1. Clone the repository

```
git clone https://github.com/JhonEsteban/Angular-Crud.git
```

2. Install the node modules

```
npm install
```

3. Start the dev server

```
ng serve -o
```

### My social networks 👋🏼

- [Linkedin](https://www.linkedin.com/in/jhon-esteban-herrera/ "My Linkendin")
